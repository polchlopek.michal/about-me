<div style='background-color: rgb(77, 76, 76);'>  


![avatar](assets/avatar256x256.png)  

</div>  


# **Hi there! 👋**
**I'm a software developer with experience in Flutter, Python, and C++. I have a passion for building high-quality applications that are efficient, maintainable, and user-friendly.**  

</br>   

# **Skills**  

**Flutter:** I am skilled in Flutter, a UI toolkit for building natively compiled applications for mobile, web, and desktop from a single codebase. I have experience building apps for Android, and the web using Flutter.

**Python:** I have experience working with Python, a versatile language used for REST servers development, data analysis, machine learning, and more. I am familiar with Django REST Framework, as well as libraries like NumPy and Pandas.

**C++:** I am proficient in C++, a powerful language used for systems programming, game development, and more. I have experience working with libraries like Boost and the Standard Template Library (STL). I participated in project launched on Linux embeded devices. 

</br>  

# **Some of my projects**
- **Online buisness card website** *[Flutter/Python(DRF)]*  
**[https://gitlab.com/polchlopek-michal-public/curriculum-vitae](https://gitlab.com/polchlopek-michal-public/curriculum-vitae)**  
Online buisness card website build in **Flutter** and **Django REST Framework**.  
The website is currenly located on [www.cv.mpolchlopek.com](https://www.cv.mpolchlopek.com) address. If you're hiring please ask me for access to ***Application*** tab 😉. 

- **Dynamic route mapper** *[Python(NumPy,DEAP)]*  
**[https://gitlab.com/polchlopek-michal-public/dynamic-route-mapper](https://gitlab.com/polchlopek-michal-public/dynamic-route-mapper)**  
The project which is a technical part of master's thesis implemented with use of **Python** with **DEAP** framework - tool frequently chosen to implement genetic alghorithms.

- **Flutter space game** *[Flutter(Flare)]*  
**[https://gitlab.com/polchlopek-michal-public/flutter-space-game](https://gitlab.com/polchlopek-michal-public/flutter-space-game)**  
The project which is a technical part of bachelor's thesis implemented in **Flutter** framework.  

- **Flutter "My Classes" project** *[Flutter]*  
**[https://gitlab.com/polchlopek-michal-public/flutter-myclasses](https://gitlab.com/polchlopek-michal-public/flutter-myclasses)**  
My experiment with animations in **Flutter** framework.  


</br>  

# **Contact**  
If you're interested in learning more about my work or would like to discuss a potential project, feel free to reach out to me at *polchlopek.michal@gmail*.com. Thanks for stopping by!